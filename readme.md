# Commerce Product Bundle Recurring

Commerce Product Bundle Recurring is created to bring Commerce Recurring functionality support for Commerce Product Bundle entities.
It also may serve as an example for bringing Commerce Recurring functionality to other entities.

After you install module

- Trait "Purchasable Entity" is added automatically to Product Bundle entity settings. Add for other entities manually if you need them to be purchased.

- 'Recurring (Product bundle)' order item type is added automatically. 
Add extra Order Item Types manually, if needed at [Order item types](admin/commerce/config/order-item-types) page and select 
"Recurring" order type for them. You may have to add [patch](https://www.drupal.org/project/commerce_recurring/issues/3225333) to have "Recurring" order type visible in dropdown.

- 'Billing schedule' and 'Subscription type' are added automatically to Product Bundle type 'Default' via Triat from first step. Add them to other Product Bundle types
manually via UI from step one, if you need.

- Manually set 'Billing schedule' and 'Subscription type' to Product Bundles you wish to be sold with recurring payments functionality. Ensure 'Billing schedule' in Product Bundle instance form is set to "Product Bundle" (not "Product Variation")

Ensure your Commerce Product Bundle version is not less than 1.0-alpha2 ( [with Traits support](https://www.drupal.org/i/3063932) )