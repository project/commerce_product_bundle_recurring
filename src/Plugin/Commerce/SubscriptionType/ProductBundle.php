<?php

namespace Drupal\commerce_product_bundle_recurring\Plugin\Commerce\SubscriptionType;
use Drupal\commerce_recurring\Plugin\Commerce\SubscriptionType\SubscriptionTypeBase;


/**
 * Provides the product bundle subscription type.
 *
 * @CommerceSubscriptionType(
 *   id = "product_bundle",
 *   label = @Translation("Product bundle"),
 *   purchasable_entity_type = "commerce_product_bundle",
 * )
 */
class ProductBundle extends SubscriptionTypeBase {}
